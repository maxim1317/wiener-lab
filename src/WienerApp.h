#include <opencv2/opencv.hpp>

using namespace cv;

const int limit        = 10000;
const int filter_limit = 7;


class WienerApp {
    public:
        WienerApp();
        ~WienerApp() = default;
        void show(Mat image);

    private:
        WienerApp(const WienerApp&) = delete;
        WienerApp& operator=(const WienerApp&) = delete;

    private:
        static void onTrackbar(int noise_slider, void* param);

    private:
        static const int slider_max_{ limit };
        static const int slider_min_{ 1 };
        static const int filter_slider_max_{ filter_limit };
        static const int filter_slider_min_{ 2 };
        int slider_{ (limit / 2) };
        int filter_slider_{ 2 };
        std::string wndTitle_{ "Wiener" };
        Mat image_;
        Mat graph_;
};