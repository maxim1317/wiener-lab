#include <opencv2/opencv.hpp>
// #include <opencv2/highgui/highgui.hpp>
// #include <opencv2/imgproc.hpp>

#include <iostream>

using namespace cv;
using namespace std;

#include "WienerFilter.h"
#include "WienerApp.h"


/**
 * @brief
 *  This main applies the Wiener Filter on a simple example image
 *
 * @return     0
 */
int main(int argc, char const *argv[])
{
    string testImage;
    if (argc == 1)
        testImage = "../pics/manylena.png";
    else
        testImage = argv[1];

    Mat1b src = imread(testImage, CV_LOAD_IMAGE_GRAYSCALE);

    if (src.empty()){
        cout << "The specified image '" << testImage << "' does not exists" << endl;
        exit(-1);
    }

    WienerApp app;
    app.show(src);
    for (int key(0); 27 != key; key = waitKey(1)) {
    }

    return 0;
}