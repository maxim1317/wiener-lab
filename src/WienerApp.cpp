#include "WienerApp.h"
#include "WienerFilter.h"
using namespace cv;


WienerApp::WienerApp() {
    namedWindow(wndTitle_, WINDOW_NORMAL | WINDOW_KEEPRATIO);
    setWindowProperty(wndTitle_, WND_PROP_ASPECT_RATIO, WINDOW_KEEPRATIO);
    createTrackbar("Noise variance: ", wndTitle_, &slider_, slider_max_ - slider_min_, onTrackbar, this);
    createTrackbar("Filter wing: ", wndTitle_, &filter_slider_, filter_slider_max_ - filter_slider_min_, onTrackbar, this);
    onTrackbar(slider_, this);
}

void WienerApp::show(Mat image) {
    image_ = image;
    onTrackbar(slider_, this);
}

void WienerApp::onTrackbar(int slider__, void* param)
{
    assert(param);
    if (0 != param) {
        WienerApp& app(*reinterpret_cast<WienerApp*>(param));
        if (app.image_.data) {
            int slider = app.slider_;
            int filter_slider = app.filter_slider_;
            double noiseVariance(slider > limit ? slider - limit : 1.0 / (limit + 1.0 - slider));
            noiseVariance = slider + 1;
            int filter_size = filter_slider * 2 + 1;
            Mat1b dst;
            WienerFilter(app.image_, dst, noiseVariance,  Size(filter_size, filter_size));

            Mat corrected = dst;

            imshow(app.wndTitle_, corrected);
        }
    }
}